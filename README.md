# Symfony test project

Just a test project.


## Pre-requisites

* PHP v5.4 or better (was tested on v5.6)
* Node v0.12 (optional, needed to build client code)


## Setup

To install all local project dependencies, run:

```
bash utils/bootstrap.sh
```

To install only the PHP part, run:

```
bash utils/bootstrap.sh php
```

Start local server:

```
php app/console server:start
```

If you edit JS or install bower components, run Gulp to bundle everything up:

```
gulp
```

Default admin credentials are `admin`: `notsosecure` and the admin page is
on `/admin`.

[Live demo].


## Notes

Project uses [LazerDB], which is a flat JSON file database. It was chosen
because it does not require a database (zero setup), has basic query
capabilities, data format is very readable, simple interface.

[Semantic UI] is a great UI framework - it has a good design out of the box,
very extensible and human-like class syntax (e.g. "tiny red fluid button").
It's a lot better compared to Bootstrap, but required a bit of work to do,
since it's not very popular in Symfony community. It also has an enormous
amount of components (over 50!), and a lot of javascript goodness.

Also, this project uses Gulp to minify all the client-side sources. Not really
needed at this point, but on later stages it gets useful.


## Contacts

Entertainer LT <[dev@ent.lt]>

[lazerdb]: https://github.com/Greg0/Lazer-Database
[semantic ui]: http://semantic-ui.com/
[live demo]: http://symtp.ent.lt/
[dev@ent.lt]: mailto:dev@ent.lt
